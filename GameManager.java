
public class GameManager {
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager()
	{	
		this.drawPile = new Deck();
		drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
	}
	
	public String toString()
	{
		String dottedLine = "------------------------------";
		String cardDrew = dottedLine + "\n" + "Center card: " + this.centerCard + "\n" + "Player card: " + this.playerCard + "\n" + dottedLine;
		return cardDrew;
	}
	
	public void dealCards()
	{
		drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		drawPile.shuffle();
		this.playerCard = drawPile.drawTopCard();
	}
	
	public int getNumberOfCards()
	{
		return drawPile.length();
	}
	
	public int calculatePoints()
	{
		if (this.centerCard.getValue().equals(this.playerCard.getValue()))
		{
			return 4;
		}
		else if (this.centerCard.getSuit().equals(this.playerCard.getSuit()))
		{
			return 2;
		}
		else
		{
			return -1;
		}
	}
		
}